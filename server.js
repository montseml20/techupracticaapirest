//como un import del framework express
var express = require('express');
var app = express();
// para decir el puerto por defecto donde escucha la api express
var port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto"+port);

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechue042692/collections/";
var mLabAPIKey ="apiKey=GXD0xn4t4l4eCUegYrrKWXCgIJMKg7nG";
var requestJson = require('request-json');
//Para parsear en formato json el contenido del body de una request
var bodyParser = require('body-parser');
var httpclient =requestJson.createClient(baseMlabURL);
var baseBinanceURL="https://api.binance.com/";
var httpclientBinance =requestJson.createClient(baseBinanceURL);
app.use(bodyParser.json());

/*Funcionalidad: Hacer login del usuario con los datos proporcionado en el body de la request
  Parametros de entrada (body):
    email
    password
  Salidas.JSON id y mensaje
    id usuario si el login ha sido satisfactorio.
    -100 El usuario ya está logado
    -200 Password incorrecto
    -300 el usuario no existe
    -400 Error en el sistema (errores en las llamadas al API de mlab)

  Se consulta en la coleccion user mediante el api de mlab si el usuario existe y si es así se valida
  si password introducido es correcto y si el usuario no estaba logado previamente.
*/
app.post('/apitechu/v2/login',
  function(req, res) {
    console.log("POST /apitechu/v2/login");

    var query = 'q={"email" : "' + req.body.email+'"}';

    httpclient.get("user?" + query + "&" +mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length > 0) {
        if (body[0].password == req.body.password) {
          if (body[0].logged) {
            console.log("Usuario "+req.body.email+" ya logado");
            response = buildJSONResponseCode(-100, "El usuario ya está logado");
            res.send(response);
          }else{
            id = body[0].id;
            query2 = 'q={"id" : ' + id +'}';
            var putBody= '{"$set":{"logged":true}}';
            httpclient.put("user?" + query2 + "&" +mLabAPIKey,
    				JSON.parse(putBody),
            function(errPut, resMLabPut, bodyPut) {
              if (errPut) {
                console.log("error actualizando usuario "+req.body.email);
                response = response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
              }else {
                console.log("usuario"+req.body.email+" logado con exito "+id);
                response = buildJSONResponseCode(id, "Usuario logado con éxito");
              }
              res.send(response);
            }
            );
          }
        }else {
          console.log("La password del usuario" +req.body.email+ "incorrecto");
          response = buildJSONResponseCode(-200, "Password incorrecto");
          res.send(response);
        }
     }else if(err){
       console.log("Error ejecutando la busqueda del usuario "+req.body.email);
       response = response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
       res.send(response);
     }else {
       console.log("No existe usuario "+req.body.email);
       response = response = buildJSONResponseCode(-300, "Usuario no existe");
       res.send(response);
     }
    });
  }
);
/*Funcionalidad: Hacer logout del usuario con id proporcionado en el body de la request
  Parametros de entrada (body):
    id del usuario que hace logout
  Salidas.JSON id y mensaje
    id usuario si el logout ha sido satisfactorio.
    -100 El usuario no está logado.
    -300 El usuario no existe.
    -400 Error en el sistema (errores en las llamadas al API de mlab)

  Se consulta en la coleccion user mediante el api de mlab si el usuario existe y si esta logado.
  Si cumple con estas condiciones se elimina el campo logged del usuario en cuestión
*/
app.post('/apitechu/v2/logout',
  function(req, res) {
    console.log("POST /apitechu/v2/logout");
    var query = 'q={"id" : ' + req.body.id+'}';
    httpclient.get("user?" + query + "&" +mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length == 1) {
        if (body[0].logged) {
          query2 = 'q={"id" : ' + req.body.id +'}';
          var putBody= '{"$unset":{"logged":1}}';
          httpclient.put("user?" + query2 + "&" +mLabAPIKey,
          JSON.parse(putBody),
          function(errPut, resMLabPut, bodyPut) {
            if (errPut) {
              console.log("error actualizando usuario "+req.body.id);
              response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
              res.send(response);
            }else {
              response = buildJSONResponseCode(req.body.id, "Logout correcto");
              res.send(response);
            }
          }
        );
      }else{
          console.log("Error el usuario no está logado");
          response = buildJSONResponseCode(-100, "El usuario no está logado");
          res.send(response);
        }
      }else if (body.length==0){
        console.log("Usuario no existe");
        response = buildJSONResponseCode(-300, "Usuario no existe");
        res.send(response);
      }
      if (err) {
        console.log("Error buscando usuario "+req.params.id);
        response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
        res.send(response);
      }
    }
    );
  }
);

/*Funcionalidad: Crear usuario con los datos proporcionados en el body de la request
  Parametros de entrada (body):
    Nombre
    Apellidos
    email
    password
  Salidas.JSON id y mensaje
    id usuario si el alta ha sido satisfactoria.
    -100 El usuario ya existe.
    -300 El usuario no existe.
    -400 Error en el sistema (errores en las llamadas al API de mlab)

  Se crea en la coleccion user mediante el api de mlab el usuario enviado desde el front. Antes de hacer la inserción se verifica que el usuario con el email enviado
  no esté dado de alta ya.
  Los id's son secuenciales. Para asignar el id a un nuevo usuario, se hace una consulta de todos los usuarios ordenada por id, se coge el mayor de ellos
  y se le suma 1.
*/
app.post('/apitechu/v2/users',
  function(req, res) {
    console.log("POST /apitechu/v2/users");
    var query = 'q={"email" : "' + req.body.email+'"}';

    httpclient.get("user?" + query + "&" +mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length==1) {
        console.log("El usuario con email "+req.body.email+" ya existe");
        response = buildJSONResponseCode(-100, "El usuario "+req.body.email+" ya existe");
        res.send(response);
      }else if (body.length==0){
        queryCalculateNextId = 'f={"id":1}&s={"id":-1}}';
        httpclient.get("user?" + queryCalculateNextId + "&" +mLabAPIKey,
        function(err2, resMLab2, body2) {
          if (body2.length > 0) {
            newUserId = body2[0].id +1;
            createNewUser = '[{"id":'+newUserId+', "first_name":"'+req.body.first_name+
            '", "last_name":"'+req.body.last_name+
            '", "email":"'+req.body.email+
            '", "password":"'+req.body.password+'"}]';

            httpclient.post("user?"+mLabAPIKey,
            JSON.parse(createNewUser),
            function(errPut, resMLabPut, bodyPut) {
              if (errPut){
                console.log("Error creando usuario");
                response = buildJSONResponseCode(-400,"Error en el sistema por favor contacte con su gestor");
              }else{
                console.log("Usuario dado de alta correctamente");
                response = buildJSONResponseCode(newUserId, "Usuario creado con éxito");
              }
              res.send(response);
            }
            );
          }else if(err){
            console.log("Error buscando email");
            response = buildJSONResponseCode(-400,"Error en el sistema por favor contacte con su gestor");
            res.send(response);
          }
        }
        );
      }
    }
    );
  }
);

/*
  Funcionalidad: Obtiene el listado de cuentas de un usuario pasado como parámetro
  Parametros de entrada (parámetro):
    id del usuario propietario de las cuentas.
  Salidas.JSON id, mensaje y lista con las cuentas si aplica.
    Listado de cuentas del usuario si la request se ha ejecutado con éxito. Puede ser vacio si el usuario no tiene cuentas,
    en ese caso se envía la lista vacia y un mensaje que informa que el usuario no tiene cuentas.
    -400 Error en el sistema (errores en las llamadas al API de mlab).
*/
app.get('/apitechu/v2/users/:id/accounts',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/accounts");

    var query = 'q={"userId" : '+req.params.id+'}';

    httpclient.get("account?" + query + "&" +mLabAPIKey,
    function(err, resMLab, body) {
   if (err) {
     console.log("error buscado cuentas del usuario "+req.params.id);
     response = buildJSONResponseCode(-400,"Error obteniendo cuentas");
     res.send(response);
   } else {
     if (body.length > 0) {
       message="";
     } else {
       console.log("usuario "+req.params.id+" no tiene cuentas creadas");
       message="El usuario no tiene cuentas por favor contacte con su gestor para crear una"
     }
     response = {
       "id":req.params.id,
       "accounts" : body,
       "message": message
     };
     res.send(response);
   }
 });
  }
);

/*
  Funcionalidad: Obtiene el listado de movimientos de una cuenta pasada como parámetro
  Parametros de entrada (parámetro):
    id de la cuienta de la que se desean buscar los movimientos.
  Salidas.JSON id, mensaje y lista con las movimientos si aplica.
    listado de movimientos de la cuenta si la request se ha ejecutado con éxito. Puede ser vacio si la cuenta no tiene movimientos,
    en ese caso se envía la lista vacia y un mensaje que informa que dicha cuenta no tiene movimientos.
    -400 Error en el sistema (errores en las llamadas al API de mlab).
*/
app.get('/apitechu/v2/accounts/:iban/transactions',
  function(req, res) {
    console.log("GET /apitechu/v2/accounts/:iban/transactions");
    var query = 'q={"iban" : "'+req.params.iban+'"}';
    httpclient.get("transactions?" + query + "&" +mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length >=0) {
        if (body.length==0) {
          message = "No tiene movimientos en esta cuenta";
        }else {
          message="";
        }
        response = {
          "id":400,
          "message":message,
          "transactions":body
        }
      }else if(err){
        console.log("Error consultando movimientos cuenta "+req.params.iban);
        response = buildJSONResponseCode(-400,"Error en el sistema por favor contacte con su gestor");
      }else {
        console.log("otros "+req.param.iban);
        response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
      }
      res.send(response);
    }
    );

  }
)

/*
  Funcionalidad: Crea los movimiento(s) y actualiza el/los saldo(s) de la(s) cuenta(s) que participen en la transacción
  que se recibe en el body de la request.

  Parametros de entrada (body):
    Iban de la cuenta origen que ordena el movimiento.
    Saldo de la cuenta origen.
    Tipo de movimiento
      1: Ingreso
      2: Cobro
      3: Transferencia
    Iban de la cuenta destino: Este campo solo vendrá informado si la operación a realizar es una transferencia.

  Salidas. JSON (id, mensaje).
      0 si el movimiento se ha creado con éxito.
    -400 Error en el sistema (errores en las llamadas al API de mlab).

    Para cada movimiento se guarda la siguiente información la colección transactions:
      - id: La fecha hora cuando se crea el movimiento en milisegundos.
      - Timestamp de creación en formato yyyy-mm-dd hh:mm:ss.mmm
      - iban de la cuenta donde se ha hecho el movimiento.
      - Tipo de movimento (1, 2 o 3)
      - Cantidad que se va a mover en esta transacción.

      Cuando llega la request si el movimiento es una transferencia se comprueba si la cuenta destino existe en la colección accounts.
      En caso positivo se crean dos movimentos y se actualizan los saldos de las dos cuentas.
      En caso de ser trasnferencia y que la cuenta destino no exista en la coleccion accounts o si el tipo de movimiento es ingreso o cobro,
      solo se crea un único movimiento en la cuenta origen se actualiza su saldo.

      La secuencia de inserciones/actualizaciones es la siguiente:
      Si es trasferencia completa (las dos cuentas exiten en la base de datos)
        Se insertan los dos movimentos
        Se actualiza saldo origen
        Se actualiza saldo destino
      Si es trasnferencia simple (solo cuenta origen exite en la base de datos) o ingreso o cobro
        Se inserta un movimiento
        Se actualiza saldo origen

      Para los rollback se ha asumido que si devuelve cualquier error el api de mlab en las inserciones o actualizaciones la operación correspondiente no se ha realizado,
      por lo que no se tiene que hacer rollback de la misma. La secuencia de operaciones a llevar a cabo en los rollback sería la siguiente:
      Si es trasferencia completa (las dos cuentas exiten en la base de datos)
        Si falla la inserción de los dos movimentos - Se asume que no ha creado nada por lo que se devuelve error pero no se hace rollback de nada.
        Si falla la actualización de saldo origen - Se hace delete de los movimientos.
        Si falla la actualización de actualiza saldo destino - Se hace delete de los movimientos y se actualiza el saldo origen a su valor original.
      Si es trasnferencia simple (solo cuenta origen exite en la base de datos) o ingreso o cobro
         Si falla la inserción del movimiento - Se asume que no ha creado nada por lo que se devuelve error pero no se hace rollback de nada.
        Si falla la actualización de saldo origen - Se hace delete del movimiento.
      Si falla la consulta del iban de la cuenta destino se devuelve un error de sistema y no continua con la creación del movimiento
*/
app.post('/apitechu/v2/transactions',
  function(req, res) {
    console.log("POST /apitechu/v2/transactions");
    if (req.body.transactionType == 3){
      queryGetDestAccount = 'q={"iban":"'+req.body.ibanDest+'"}';
      httpclient.get("account?" + queryGetDestAccount + "&" +mLabAPIKey,
      function(errGetDestAccount, resMLabGetDestAccount, bodyGetDestAccount) {
        if (bodyGetDestAccount.length==1) {
          //Si es 0 quiere decir que la cuenta no es de este banco.
          createTransaction(req, bodyGetDestAccount, res);
        }else if ((bodyGetDestAccount.length==0)) {
          createTransaction(req, [], res);
        }else if((errGetDestAccount)||(bodyGetDestAccount.length!=0)) {
          //En ese caso hacemos transferencia solo en cuenta origen.En cualquier otro caso
          //asumimos que hay un error y el cliente debe contactar con su gestor para solucionarlo.
          console.log("Error buscando cuenta destino");
          response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
          res.send(response);
        }
      });
    }else {
      console.log("llamo a crear transaccion");
      //Se pasa el control de la request a la funcion createTransaction porque si no la funcion resultante con todos lo insert, update, rollback, etc se hace
      //muy difícil de seguir
      createTransaction(req, [], res);
    }
});

/*
  Funcionalidad: Crea el/los movimiento(s) correspondiente(s) y actualiza el/los saldo(s)
  Parametros de entrada:
    - Body de la request
    - Datos de la cuenta destino que habremos recuperado de la consulta a la base de datos hecha anteriormente.
      Esto solo estará relleno si el moviento es uns trasferencia entre dos cuentas existentes en la base de datos.
    - Variable res que devuelve la respuesta al dataManger que llama a la API
*/
function createTransaction(bodyReq, accountDestData, resRequest){
  idTrOrig= generateTimestampId();
  createNewTransaction = '[{"id":'+idTrOrig+', "timestamp":"'+idTrOrig.format('YYYY-MM-DD HH:mm:ss.SSS')+
  '","iban":"'+bodyReq.body.iban+
  '", "transactionType":'+bodyReq.body.transactionType+
  ', "amount":'+bodyReq.body.amount+'}';
  idTrDest=0
  if ((bodyReq.body.transactionType == 3) && (accountDestData.length==1)){
    idTrDest= generateTimestampId();
    createNewTransaction =createNewTransaction +',{"id":'+idTrDest+', "timestamp":"'+idTrDest.format('YYYY-MM-DD HH:mm:ss.SSS')+
    '","iban":"'+bodyReq.body.ibanDest+
    '", "transactionType":'+bodyReq.body.transactionType+
    ', "amount":'+bodyReq.body.amount+'}';
  }

  createNewTransaction = createNewTransaction + "]";
  httpclient.post("transactions?"+mLabAPIKey,
  JSON.parse(createNewTransaction),
  function(errInsertTransaction, resMLabInsertTransaction, bodyInsertTransaction) {
    if (errInsertTransaction) {
      console.log("Error insertando movimiento "+errInsertTransaction);
      response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
      resRequest.send(response);
    }else {
      //actualizar saldo.
      newBalanceOrigAccount=calcularSaldoCuentaOrig(bodyReq.body.transactionType,bodyReq.body.balance,bodyReq.body.amount);
      if (newBalanceOrigAccount!=null){
        //actualizo saldo
        queryUpdateOrigAccountBalance = 'q={"iban":"'+bodyReq.body.iban+'"}';
        putOrigAccountBalance = '{"$set":{"balance":'+newBalanceOrigAccount+'}}';
        httpclient.put("account?" + queryUpdateOrigAccountBalance + "&" +mLabAPIKey,
        JSON.parse(putOrigAccountBalance),
        function(errPutOrigAccountBalance, resMLabPutOrigAccountBalance, bodyPutOrigAccountBalance) {
          if (errPutOrigAccountBalance) {
            //rollback movimiento o movimientos si es transferencia.
            rollbackTransaction(idTrOrig,idTrDest,"",0,resRequest);
          }else if((bodyReq.body.transactionType==3)&&(accountDestData.length==1)) {
            newBalanceDestAccount = parseFloat(accountDestData[0].balance) + parseFloat(bodyReq.body.amount);
            queryUpdateDestAccountBalance = 'q={"iban":"'+bodyReq.body.ibanDest+'"}';
            putDestAccountBalance = '{"$set":{"balance":'+newBalanceDestAccount+'}}';
            httpclient.put("account?" + queryUpdateDestAccountBalance + "&" +mLabAPIKey,
            JSON.parse(putDestAccountBalance),
            function(errPutDestAccountBalance, resMLabPutDestAccountBalance, bodyPutDestAccountBalance) {
                if (errPutDestAccountBalance) {
                  //Rollback completo movimientos y saldo origen
                  rollbackTransaction(idTrOrig,idTrDest,bodyReq.body.iban,bodyReq.body.balance,resRequest);
                }else{
                  response = buildJSONResponseCode(0,"Movimiento creado con éxito");
                  resRequest.send(response);
                }
            });
          }else{
            //ok saldo origen y no transferencia con cuenta del banco. Movimiento insertado con exito.
            response = buildJSONResponseCode(0, "Movimiento creado con éxito");
            resRequest.send(response);
          }
        });
      }else {
        //Error actualizando el saldo origen
        console.log("Error actualizando saldo origen");
        response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
        resRequest.send(response);
      }
    }
  });
}

/*
  Funcionalidad: Deshace el/los movimiento(s) o actualizaciones el/los saldo(s) que se hayan hecho previamente al error del api de mlab.
  Parametros de entrada:
    - id del movimiento a eliminar en la cuenta origen
    - id del movimiento a eliminar en la cuenta destino (este parámetro sera 0 si no es trasnferencia completa).
    - Iban de la cuenta origen de la que habrá que hacer rollback del update del saldo. Si es vacío significa que no es neceasarío hacer rollback.
    - Saldo original de la cuenta origen.
    - Variable res que devuelve la respuesta al dataManger que llama a la API
*/

function rollbackTransaction(idTransactionOrig,idTransactionDest,accountToRollback,originalBalance,resToSend){

//Si el idTransactionDest es 0 significa que no es transferencia completa. Por lo que solo hay que borrar un movimiento.
  if (idTransactionDest!=0){
    queryIdToDelete='q={$or:[{"id":'+idTransactionOrig+'},{"id":'+idTransactionDest+'}]}';
  }else{
    queryIdToDelete='q={"id":'+idTransactionOrig+'}';
  }
  //body vacio. Haciendo un put con una lista vacia la api de mlab borra los registros resustantes de la consulta anterior
  putBody='[{}]';
  httpclient.put("transactions?"+ queryIdToDelete +"&"+ mLabAPIKey,
  JSON.parse(putBody),
  function(errDelete, resMLabDelete, bodyDelete) {
    if (errDelete){
      console.log("Error borrando movimientos "+idToDelete);
      response = buildJSONResponseCode(-400,"Error en el sistema por favor contacte con su gestor");
      resToSend.send(response);
    }
    else {
      //Se han borrado correctamente los movimientos. Comprobar si hay que hacer rolback del saldo de la cuenta origen
      if (accountToRollback!=""){
        queryRollbackBalance = 'q={"iban":"'+accountToRollback+'"}';
        putRollbackBalance = '{"$set":{"balance":'+originalBalance+'}}';
        httpclient.put("account?"+ queryRollbackBalance +"&"+ mLabAPIKey,
          JSON.parse(putRollbackBalance),
          function(errPut, resMLabPut, bodyPut) {
            if (errPut) {
              console.log("Error haciendo rollback saldo cuenta original iban: "+accoutToRollback);
              response = buildJSONResponseCode(-400,"Error en el sistema por favor contacte con su gestor");
            }else {
              //rollback del saldo hecho correctamente
              console.log("Error rollback Saldo. El movimiento no se ha podido realizar");
              response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
            }
            resToSend.send(response);
          });
        }else{
          //rollback de/los movimiento(s) hecho correctamente
          console.log("Rollback solo alta movimiento. El movimiento no se ha podido realizar");
          response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
          resToSend.send(response);
        }
      }
    });
}

/*funcion para generar los id's de los movmientos.
  Salida
    -Timestamp de creación del movimiento en milisegundos*/
function generateTimestampId() {
  var moment = require('moment');
  return moment();
}

/*Funcionalidad: calcula el nuevo saldo de la cuenta origen teniendo en cuenta el tipo de movimiento
  Parametros de entrada:
      - Tipo de transaccion
          1: Ingreso (se incrementa el saldo)
          2: Cobro (se reduce el saldo)
          3: Transferencia (se reduce el saldo)
      - Saldo a actualizar.
      - Cantidad del movimento (se suma o resta al saldo en función del tipo de movimiento)
    Salida:
      - Saldo actualizado o null si hay algun error
      */
function calcularSaldoCuentaOrig(transactionType, balanceOrigAccount,transactionAmount){

  floatTransactionAmount = parseFloat(transactionAmount);
  floatbalanceOrigAccount =  parseFloat(balanceOrigAccount);

  switch(transactionType){
    case 1:
          return balanceOrigAccount + floatTransactionAmount;
    case 2:
          return floatbalanceOrigAccount - floatTransactionAmount;
    case 3:
          return floatbalanceOrigAccount - floatTransactionAmount;
    default:
          console.log("Error tipo de movimiento incorrecto");
          return null;
   }
}

/*Función que construye la variable JSON que se devuelve al dataManager que hacer la request a la API
  Parámetros de entrada
    - Código o id númerico que indican si la request ha ido bien o no
    - Mensaje a mostras al usuario.
  */
function buildJSONResponseCode(id, message){
  response = {
    "id" : id,
    "message": message
  };
  return response;
}

//BINANCE API

/*Funcionalidad custom. Obtención del datos estádisticos de las últimas 24 horas de 3 criptodivisas que ofrece api de Binance(empresa que permite operar con criptodivisas)
  No tiene parámetros de entrada. Se han elegido 3 de las criptomonedas con más capitalización cruzadas con el bitcoin (Litecoin, Ethereum y Ripple).
  Salida
    - Devuelve una lista con la información mas relevante en las últimas 24 (desde la hora que se hace la request) de los pares de criptomonedas LTCBTC, ETHBTC y XRPBTC
     -400 si ha habido algún error en la llamada al api de binace */
app.get('/apitechu/v2/cryptocurrency',
  function(req, res) {
    console.log("GET /apitechu/v2/cryptocurrency");
    var results = [];
    baseRequestTicker24hr="api/v1/ticker/24hr?symbol="
    var i=0;
    httpclientBinance.get(baseRequestTicker24hr+"LTCBTC",
      function(err, resBinace, body) {
        if(err){
          console.log("Error getting info LTCBTC");
          response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
          res.send(response);
        }else {
          console.log("No Error getting info LTCBTC");
          results[i]=body;
          i=i+1;
          httpclientBinance.get(baseRequestTicker24hr+"ETHBTC",
            function(err2, resBinace2, body2) {
              if(err2){
                console.log("Error getting info ETHBTC");
                response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
                res.send(response);
              }else {
                console.log("No Error getting info ETHBTC");
                results[i]=body2;
                i=i+1;
                httpclientBinance.get(baseRequestTicker24hr+"XRPBTC",
                  function(err3, resBinace3, body3) {
                    if(err3){
                      console.log("Error getting info XRPBTC");
                      response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
                      res.send(response);
                    }else {
                      console.log("No Error getting info XRPBTC");
                      results[i]=body3;
                      response = {
                        "id":400,
                        "message":"",
                        "cryptoInfo":results
                      }
                      res.send(response);
                    }
                  });
              }
            });
        }
      });
  }
);

/*Funcionalidad custom. Obtiene el precio actual de mercado del par de criptodivisas pasado como parametro.
/*Parámetro de entrada
  Par de de divisas para el cual se quiere consultar el precio. Se han elegido 3 de las criptomonedas con más capitalización cruzadas con el bitcoin (Litecoin, Ethereum y Ripple).
  Salida
      Precio actual de mercado del par proporcionado como parámetro.
     -400 si ha hbaido algún error en la llamada al api de binace
*/
app.get('/apitechu/v2/cryptocurrency/:symbol',
  function(req, res) {
    console.log("GET /apitechu/v2/cryptocurrency:symbol");
    httpclientBinance.get("api/v3/ticker/price?symbol="+req.params.symbol+"",
      function(err, resBinace, body) {
        if (err){
          console.log("Error obteniendo precio criptodivisa: "+req.params.symbol);
          response = buildJSONResponseCode(-400, "Error en el sistema por favor contacte con su gestor");
          res.send(response);
        }else{
          response={
            "id":400,
            "message":"",
            "price":body.price
          }
          res.send(response);
        }
      });

  }
);
