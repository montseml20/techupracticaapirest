var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

var server = require('../server.js');
chai.use(chaihttp);

var should = chai.should();

describe('First test',
  function(){
    it('Test that DuckDuckGo works', function(done){
      //funciona http://www.duckduckgo.com devuelve 200
      chai.request('http://www.duckduckgo.com')
      //falla https://developer.mozilla.org/asa devuelve 404
      //chai.request('https://developer.mozilla.org/asa')
      .get('/')
      .end(
        function(err, res) {
          console.log("Request has ended");
          //console.log(res);
          //res.should tiene que tener una propiedad llamada status que sea 200
          res.should.have.status(200);
          console.log(err);
          done();
        }
      );
    });

  }
);

describe('Test de API Usuarios',
 function() {
   //se pueden poner N elementos test
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("Hola desde apitechu")
             done();
           }
         )
     }
   )
   it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (user of res.body) {
             console.log(user.id);
             console.log('jajaajajajaj');
             user.should.have.property('id');
             user.should.have.property('first_name');
             user.should.have.property('last_name');
             user.should.have.property('email');
             user.should.have.property('country');
           }
           done();
         }
       )
     }
   )
 }
);
