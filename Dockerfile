#Comentarios
#Imagen Raiz
FROM node

#Carpeta raiz
WORKDIR /apitechufinal

# Copia de archivos. Lo que haya en la carpeta /home/alumno/Escritorio/projects/esquema_proyecto a apitechu
ADD . /apitechufinal

#Exponer puerto
EXPOSE 3000

#Instalar las dependencias
#RUN npm install

#Comando de inicialización
CMD ["npm", "start"]
